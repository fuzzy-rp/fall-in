import Phaser from "./lib/phaser.js";
import Game from "./scenes/game.js";
import GameOver from "./scenes/gameOver.js";
import Pause from "./scenes/pause.js";

export default new Phaser.Game({
    type: Phaser.AUTO,
    width: 480,
    height: 640,
    scene: [Game, GameOver, Pause],
    parent: "game",
    physics: {
        default: "arcade",
        arcade: {
            gravity: {
                y: 200,
            },
            // debug: true,
            // debugShowBody: true,
            // debugShowVelocity: true,
        },
    },
});
