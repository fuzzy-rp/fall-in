export const coinTypes = ["gold", "silver", "bronze"];

export class Coin extends Phaser.Physics.Arcade.Sprite {
    /**
     *
     * @param {Phaser.Scene} scene
     * @param {number} x
     * @param {number} y
     * @param {string} texture
     * @param {string} frame
     */
    constructor(scene, x, y, texture, frame, coinType) {
        super(scene, x, y, texture, frame);
        if (!coinType) {
            coinType = coinTypes[Math.floor(Math.random() * 3)];
        }
        this.play(`${coinType}_coin_spin`, true).setScale(0.25);
    }
}
