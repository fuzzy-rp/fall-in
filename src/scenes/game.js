import Phaser from "../lib/phaser.js";
import { Coin, coinTypes } from "../game/coin.js";

export default class Game extends Phaser.Scene {
    /** @type {Phaser.GameObjects.Graphics} */
    debugGraphics;

    /** @type {Phaser.Physics.Arcade.Sprite} */
    player;

    /** @type {Phaser.Physics.Arcade.StaticGroup} */
    platforms;

    /** @type {Phaser.Physics.Arcade.Group} */
    coins;

    cursors;

    rows = 4;
    cols = 6;
    tileScale = 1 / 4;

    rowDist = 0;
    colWidth = 0;
    scrollSpeed = 2;

    setInternals() {
        this.rowDist = this.cameras.main.displayHeight / this.rows;
        this.colWidth = this.cameras.main.displayWidth / this.cols;
        this.xStart = this.colWidth / 2;
    }

    constructor() {
        super("game");
    }

    init() { }

    preload() {
        this.load.image("background", "assets/bg_layer1.png");
        this.load.image("platform", "assets/platformPack_tile001.png");
        this.load.image("white-smoke", "assets/white-smoke.png");

        this.cursors = this.input.keyboard.addKeys({
            left: Phaser.Input.Keyboard.KeyCodes.A,
            right: Phaser.Input.Keyboard.KeyCodes.D,
            up: Phaser.Input.Keyboard.KeyCodes.SPACE,
        });

        this.input.keyboard.on("keydown-P", () => {
            console.error("sleeping");
            this.scene.sleep();
            this.scene.launch("pause", this.scene.key);
        });

        this.load.atlasXML(
            "tiles",
            "assets/spritesheet_jumper.png",
            "assets/spritesheet_jumper.xml"
        );
    }

    create() {
        this.setInternals();

        this.add.image(240, 320, "background").setScrollFactor(1, 0);
        this.anims.create({
            key: "walk",
            frames: [
                { key: "tiles", frame: "bunny1_walk1.png", duration: 50 },
                { key: "tiles", frame: "bunny1_walk2.png", duration: 50 },
            ],
            frameRate: 8,
            repeat: -1,
        });

        const coinFrameDuration = 150;
        for (const coinType of coinTypes) {
            this.anims.create({
                key: `${coinType}_coin_spin`,
                frames: this.anims.generateFrameNames("tiles", {
                    prefix: `${coinType}_`,
                    suffix: ".png",
                    start: 1,
                    end: 4,
                }),
                duration: coinFrameDuration,
                frameRate: 8,
                repeat: -1,
                yoyo: true,
            });
        }

        this.debugGraphics = this.add.graphics();
        this.debugGraphics.alpha = 0;
        this.input.keyboard.on("keydown-ENTER", () => {
            console.error("toggling debug");
            this.debugGraphics.alpha = 1 - this.debugGraphics.alpha;
        });

        const { rows, cols } = this;
        const camera = this.cameras.main;
        const { width, height } = camera;
        const bottom = camera.scrollY + camera.displayHeight;
        const right = camera.scrollX + camera.displayWidth;

        this.coins = this.physics.add.group({
            classType: Coin,
        });

        this.platforms = this.physics.add.staticGroup();
        for (let i = 0; i < rows; i++) {
            this.addRow(i);
        }

        const player = this.physics.add
            .sprite(240, 320, "tiles", "bunny1_stand.png")
            .setScale(0.3)
            .setDepth(100);
        this.player = player;

        this.physics.add.collider(this.platforms, this.player);
        this.physics.add.collider(this.platforms, this.coins);
        this.physics.add.overlap(
            this.player,
            this.coins,
            this.handleCollectCoin,
            undefined,
            this
        );
    }

    addRow(i) {
        const { cols } = this;
        const holes = Phaser.Math.Between(1, Math.ceil(cols / 2));
        const holeIndexes = [];
        do {
            const r = Phaser.Math.Between(0, cols - 1);
            if (holeIndexes.indexOf(r) < 0) {
                holeIndexes.push(r);
            }
        } while (holeIndexes.length < holes);

        for (let j = 0; j < cols; j++) {
            if (holeIndexes.indexOf(j) < 0) {
                this.createPlatform(i, j);
            }
        }
    }

    createPlatform(i, j) {
        const { platforms, tileScale, colWidth, rowDist, xStart } = this;
        const x = xStart + j * colWidth;
        const y = i * rowDist;
        const height = 64 * tileScale;
        const platform = this.add.tileSprite(
            x,
            y,
            colWidth,
            height,
            "platform"
        );
        platforms.add(platform);
        platform.setTileScale(tileScale, tileScale);
        platform.body.updateFromGameObject();
        this.addCoinAbove(platform);
    }

    update() {
        this.camera = this.cameras.main;
        const {
            camera,
            player,
            particleEmitter,
            platforms,
            debugGraphics,
            cursors,
            rows,
            scrollSpeed,
        } = this;

        this.setInternals();

        debugGraphics.clear();
        debugGraphics.lineStyle(2, 0xff0000);

        this.cameras.main.scrollY += scrollSpeed;

        if (cursors.up.isDown && player.body.touching.down) {
            player.setVelocityY(-200);
        }
        if (cursors.left.isDown) {
            player.setVelocityX(-200);

            if (!player.flipX) {
                player.setTexture("tiles", "bunny1_walk1.png").play("walk");
                player.flipX = true;
            }
        } else if (cursors.right.isDown) {
            player.setVelocityX(200);
            if (player.flipX) {
                player.setTexture("tiles", "bunny1_walk1.png").play("walk");
                player.flipX = false;
            }
        } else {
            player.setVelocityX(0);
            player.setTexture("tiles", "bunny1_stand.png");
            player.flipX = false;
        }

        const bottom = camera.scrollY + camera.displayHeight;
        const top = camera.scrollY;

        debugGraphics.lineStyle(1, "red");
        debugGraphics.lineBetween(0, bottom, camera.displayWidth, bottom);
        debugGraphics.lineStyle(1, "green");
        debugGraphics.lineBetween(0, top, camera.displayWidth, top);

        let shouldBounce = player.body.touching.down;
        shouldBounce = false;
        if (player.body.velocity.y === 0 && player.y < top) {
            player.y += top - player.y;
        }
        if (player.y + player.displayHeight / 2 > bottom) {
            shouldBounce = true;
        }

        if (shouldBounce) {
            player.setVelocityY(-200);
        }

        this.horizontalWrap(player);

        const platformYs = platforms.children.entries.map((c) => c.body.y);
        const lowestPlatformY = Math.max(...platformYs);
        const shouldAddRow = lowestPlatformY < bottom - this.rowDist;
        if (shouldAddRow) {
            const rowIndex = Math.floor(bottom / this.rowDist) + 1;
            this.addRow(rowIndex);
        }
        const offScreenPlatforms = [];
        platforms.children.iterate((child) => {
            if (child.body.y < top - 100) {
                offScreenPlatforms.push(child);
            }
        });
        offScreenPlatforms.forEach((p) => {
            this.platforms.killAndHide(p);
            this.platforms.remove(p);
        });

        this.coins.children.iterate((coin) => {
            if (coin.y < top - 100) {
                this.handleCollectCoin(undefined, coin);
            }
        });
    }

    /**
     *
     * @param {Phaser.GameObjects.Sprite} sprite
     */
    horizontalWrap(sprite) {
        const halfWidth = sprite.displayWidth * 0.5;
        const gameWidth = this.scale.width;
        if (sprite.x < -halfWidth) {
            sprite.x = gameWidth + halfWidth;
        } else if (sprite.x > gameWidth + halfWidth) {
            sprite.x = -halfWidth;
        }
    }

    /**
     *
     * @param {Phaser.GameObjects.Sprite} sprite
     */
    addCoinAbove(sprite) {
        const y = sprite.y - sprite.displayHeight;

        /** @type {Phaser.Physics.Arcade.Sprite} */
        const coin = this.coins.get(sprite.x, y, "tiles", "coin_gold.png");

        coin.setActive(true);
        coin.setVisible(true);
        coin.body.setSize(coin.width, coin.height);
        coin.anims.restart();
        this.add.existing(coin);
        this.physics.world.enable(coin);
        return coin;
    }

    /**
     *
     * @param {Phaser.Physics.Arcade.Sprite} player
     * @param {Coin} coin
     */
    handleCollectCoin(player, coin) {
        /** @type {Phaser.Types.Tweens.TweenBuilderConfig} */
        const tweenConfig = {
            targets: coin,
            y: coin.y - 100,
            alpha: 0,
            duration: 800,
            ease: "Cubic.easeOut",
            callbackScope: this,
            onComplete: () => {
                this.coins.killAndHide(coin);
                this.coins.remove(coin);
            },
        };
        this.tweens.add(tweenConfig);
    }
}
